import logging
import Fibonacci

FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)


def print_hi(name):
    logging.info(f'{name}')


print_hi('Hallo Welt!')

# fib = Fibonacci.fibonacci1(1)
# if fib is not None:
#     logging.info(fib)
# else:
#     logging.error("something went wrong")
