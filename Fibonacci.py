import logging

def fibonacci1(n):
    a = 0
    b = 1
    if n <= 0:
        logging.error("Incorrect input")
    elif n == 1:
        return b
    else:
        for i in range(2,n):
            c = a + b
            a = b
            b = c
        return b

def fibonacci2(n):
    if n <= 0:
        logging.error("Incorrect input")
    # First Fibonacci number is 0
    elif n == 1:
        return 0
    # Second Fibonacci number is 1
    elif n == 2:
        return 1
    else:
        return Fibonacci2(n - 1) + Fibonacci2(n - 2)
